# ![civilise logo](Resources/Artboard_1ldpi.png)
### Look. Dream. Solve. 

Table of Contents
=================

  * [Introduction](#introduction)
  * [Customer Understanding](#customer-understanding)
  * [Team](#team)
  * [System](#system)
  * [Progress and Planning](#progress-and-planning)
  * [Linkdump](#linkdump)
  * [Access](#access)

Created by [gh-md-toc](https://github.com/ekalinin/github-markdown-toc)

## Introduction
Oak is a system to produce and score urban planning designs. Work on the project began on February 24th, 2018. This is the Audit Landing page and is a repository of the latest updates for the project. Documentation and code is in our GitLab [repositories](https://gitlab.com/civilise-ai) and we collaborate online via [slack](https://civilise-ai.slack.com). To gain access to any of these platforms please email John Forbes through john.forbes@civilise.ai. For further details on our current stage of progress there is an audit report and presentation found in this repository here ([Audit](Audit) folder).

## Stakeholders
Civilise.ai is a company owned by John Forbes. Civilise.ai's focus is to bring emerging technologies into the infrastructure and planning industry to provide improved, sustainable and informative packages to its clients.

The Oak Project provides value through assessed designs and metrics never before implemented in urban design.
 
 
## Team
Our team meets regularly in person on Wednesday for tutorials and Saturdays for group work. 

Team roles are defined as follows:

| Team Member            | Dev Role           | 
| -----------------------| -------------------| 
| Yue Lian               | Metrics            |
| Jeh-Chun (Johnny) Yang | Metrics            | 
| Nicholas Chua          | Team Lead          | 
| Geoffrey Dwyer         | Database           |
| Sebastian Lau          | Generation         |
| Ted Pettigrove         | Constraints        |

## Subproject Goals

# ![Subproject Goals](Resources/subproject goals.PNG)

## System

# ![System Diagram](Resources/System.png)



## Progress and Planning

All progress and tasks can be found on our git issues in the Civilise.ai offical gitlab.



## Linkdump
#### [Audit report](Audit/Report.pdf)
#### [Audit slides](Audit/SlidesA2.pdf)
#### [Meeting Notes](Meeting Notes)
#### [Resources](Resources)
#### [Repos](https://gitlab.com/civilise-ai)
#### [Drive](https://drive.google.com/drive/folders/1kFD8vAI5kDc9IkoJRm_TsOoJZ3WMn7Hb?usp=sharing)

## Access
You may ask us (via email, etc) to give access permission to your own Gitlab.com account or use the following dummy account to access the repositories.

Please adhere to the following guidelines when using the dummy account:
- Do **NOT** use the dummy account for purposes other than accessing the Civilise.ai repositories.
- Do **NOT** attach any personal/extra information to the dummy account, this includes filling in personal information, personal email addresses, linking to accounts on other sites.
- Do **NOT** modify the account attributes of the dummy account, this includes email address, account password.

```
user name : civdummy2
password  : h2(,3P~Jfgda=PKY^U+BN!:Pcsn_`~
```
